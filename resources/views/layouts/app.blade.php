<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Gestão de Utilizadores</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    </head>
    <body>
        <div class="container">
            <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
              <a href="/" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
                <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
                <img src="{{asset('img/logo.png')}}" class="d-flex col-md-3 mb-2 mb-md-0" width="40" height="32">
              </a>

              <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
                <li><a href="/" class="nav-link px-2 link-secondary">Home</a></li>
                <li><a href="#" class="nav-link px-2 link-dark">Gestão de Utilizador</a></li>
                <li><a href="#" class="nav-link px-2 link-dark">Gestão de Produtos</a></li>
                <li><a href="#" class="nav-link px-2 link-dark">Gestão de Compras</a></li>
                <li><a href="#" class="nav-link px-2 link-dark">Sobre</a></li>
              </ul>

              <div class="col-md-3 text-end">
                <button type="button" class="btn btn-outline-primary me-2">Iniciar Sessão</button>
                <button type="button" class="btn btn-primary">Registar</button>
              </div>
            </header>
            @yield('content')
  </div>

        <script type="text/javascript" src="{{mix('js/app.js')}}"></script>
    </body>
</html>
